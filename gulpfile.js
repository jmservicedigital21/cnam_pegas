const gulp = require("gulp");
const sass = require("gulp-sass");
const cssnano = require("gulp-cssnano");
const browserSync = require("browser-sync").create();

// build style to static media folder
function style_dev() {
  return gulp
    .src("./src/ui/**/main.scss")
    .pipe(sass())
    .pipe(gulp.dest("./static"))
    .pipe(browserSync.stream());
}

// build style to storybook-static folder
function style_prod() {
  return gulp
    .src("./src/ui/**/*.scss")
    .pipe(sass())
    .pipe(cssnano())
    .pipe(gulp.dest("./storybook-static"));
}

function watch() {
  browserSync.init({
    proxy: "http://localhost:9006",
  });
  gulp.watch("./src/ui/**/*.scss", style_dev);
}

exports.style_dev = style_dev;
exports.style_prod = style_prod;
exports.watch = watch;
