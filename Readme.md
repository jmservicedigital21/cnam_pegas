# Design System Pega Cnam


Ce projet repose sur Storybookjs https://storybook.js.org

## Elément requis avant l'installation

Nodejs https://nodejs.org/uk/blog/release/v12.19.0/ ou supérieur (de préférence une version LTS)
Gulp https://gulpjs.com/docs/en/getting-started/quick-start

## Installation

Une fois le projet téléchargé sur votre machine depuis Gitlab vous devez utiliser les commandes suivantes pour l'installer :

```sh
npm install
ou
yarn (si vous l'utilisez)
```

---

## Démarrage du projet

Différentes commandes doivent être exécuté pour lancer le projet.
La première permet le démarrage de storybook en arrière plan:
```sh
npm run storybook
ou
yarn storybook
```
Important, la première commande doit s’exécuter correctement pour pouvoir lancer la seconde commande.
La deuxième permet le démarrage de Browsersync https://browsersync.io/ qui lui ouvre une fenêtre `http://localhost:3000/`, observe les changements effectués sur les fichiers `.scss`, recompile le fichier `main.css` et rafraîchie l'affichage :
```sh
gulp watch
```
