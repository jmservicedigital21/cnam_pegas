export default {
  title: "01-Design System/Composants Cnam/Champs de formulaire",
  argTypes: {
    label: { type: { name: "string" } },
    classesInput: { type: { name: "string" } },
  },
};

const Template = ({
  label,
  classesInput,
  disabled = false,
}) => `<label
  data-test-id=""
  class="field-caption dataLabelForWrite icon-required "
  for="0f62e4ec"
  data-required=""
>
  ${label}
</label>
<div class="field-item dataValueWrite"><span nowrap="">
<input ${
  disabled ? "disabled" : ""
} data-template="" data-change="" data-keydown="" data-ctl="" type="text" id="0f62e4ec" value="" class="leftJustifyStyle ${classesInput}" title="Veuillez saisir le nom de famille de l'assuré" name="" placeholder="Saisir le nom de famille" aria-describedby="" aria-label="Veuillez saisir le nom de famille de l'assuré" data-changed="">
</span></div>
`;

const TemplateNb = ({
  label,
  classesInput,
  disabled = false,
}) => `<label>${label}:</label>
  <div class="field-item dataValueWrite">
    <span nowrap="">
      <input ${
        disabled ? "disabled" : ""
      } id="exemple-number" type="number" class="${classesInput}">
    </span>
  </div>
`;

const TemplateTextArea = ({
  label,
  classesInput,
  disabled = false,
}) => `<label>${label}:</label>
  <div class="field-item dataValueWrite">
    <span nowrap="">
    <textarea ${disabled ? "disabled" : ""} class="${classesInput}" rows="10"></textarea>
    </span>
  </div>
`;

export const Champ = Template.bind({});
Champ.args = { label: "Nom*", classesInput: "form-control", disabled: false };
Champ.storyName = "Champ de texte";

export const ChampNb = TemplateNb.bind({});
ChampNb.args = {
  label: "Libellé*",
  classesInput: "form-control",
  disabled: false,
};
ChampNb.storyName = "Champ numérique";

export const ChampTextArea = TemplateTextArea.bind({});
ChampTextArea.args = {
  label: "Libellé*",
  classesInput: "form-control",
  disabled: false,
};
ChampTextArea.storyName = "Champ textarea";
