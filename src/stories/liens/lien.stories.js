import md from "./lien.md";

export default {
  title: "01-Design System/Composants Cnam/Liens",
  parameters: {
    docs: {
      description: {
        component: md,
      },
    },
  },
  argTypes: {
    text: { type: { name: "string" } },
  },
};

const Template = ({ text = 'Exemple de lien' }) => `<a href="#">${text}</a>`;

export const Link = Template.bind({});
Link.storyName = "Lien";

