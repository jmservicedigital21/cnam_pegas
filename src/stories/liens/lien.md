#### Règles d’usage :

Les liens respectent la même couleur des boutons primaires à l’état normal, au survol, ils deviennent soulignés . Dans les cas des liens externes, vous devez ajouter un pictogramme informatif à droit du libellé.
