import md from "./bouton-primaire.md";

export default {
  title: "01-Design System/Composants Cnam/Boutons/Primaire",
  parameters: {
    docs: {
      description: {
        component: md,
      },
    },
  },
  args: {
    text: "TAILLE MOYENNE",
    disabled: false,
  },
  argTypes: {
    text: { type: { name: "string" } },
  },
};

const Template = ({ text, classes, disabled = false }) =>
  `<button class="btn btn-primary ${classes}"
  ${disabled ? "disabled" : ""}>${text}</button>`;

export const ButtonDefault = Template.bind({});
ButtonDefault.args = { classes: "" };
ButtonDefault.storyName = "Default";

export const ButtonLarge = Template.bind({});
ButtonLarge.args = { classes: "btn-large"};
ButtonLarge.storyName = "Moyen";

export const ButtonBig = Template.bind({});
ButtonBig.args = { text: "TAILLE LARGE", classes: "btn-big" };
ButtonBig.storyName = "Large";
