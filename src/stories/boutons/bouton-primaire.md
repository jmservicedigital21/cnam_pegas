#### Règles d’usage :

Utilisez les boutons primaires lorsque vous voulez donner plus d’importance à une action par rapport à autre, ou lorsque vous avez besoin de distinguer une action positive d’une négative. Les boutons primaires ont plus de visibilité que les secondaires et sont toujours positionnés à droite (même quand si utilisés de manière isolée), ou au-dessus du bouton secondaire sur smartphone.

Dans les cas où une seule action est nécessaire, utilisez le style primaire, sauf si l’action n’est pas importante, par exemple “Fermer” ou “annuler”.