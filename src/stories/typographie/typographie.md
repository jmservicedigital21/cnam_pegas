#### Règles d’usage :

2 polices de caractères sont utilisées dans les interfaces amelipro :

OSP-DIN pour les titres de services.

Open sans pour tous les autres textes : autres titres, paragraphes, éléments d’interactions, composants, etc. Cette police est utilisée avec 2 graisses : regular et semibold.

# Titre h1
Placé dans l’entête de page, aligné à gauche ou centré sur les écrans de type smartphone. De couleur Bleu Azur profond. Toujours en majuscule. En 2 tailles selon le device.

# Titre h2
Le titre ```h2``` est utilisé pour les titres de panel.

# Titre h3
Le titre ```h3``` est utilisé pour les sous-titres de panel.

# Titre h4
Le titre ```h4``` est utilisé pour les titres des Pop-in.