import md from "./typographie.md";

export default {
  title: "01-Design System/Composants Cnam/Typographie",
  parameters: {
    docs: {
      description: {
        component: md,
      },
    },
  },
  args: {
    textParagraph:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
};

export const Title1 = ({ text }) => `<h1>${text}</h1>`;
Title1.args = { text: "TITRE DE SERVICE" };
Title1.storyName = "Titre h1";

export const Title2 = ({ text }) => `<h2>${text}</h2>`;
Title2.args = { text: "TITRE DE PAGES" };
Title2.storyName = "Titre h2";

export const Title3 = ({ text }) => `<h3>${text}</h3>`;
Title3.args = { text: "Sous-titres" };
Title3.storyName = "Titre h3";

export const Title3Uppercase = ({ text, classes }) =>
  `<h3 class="${classes}">${text}</h3>`;
Title3Uppercase.args = {
  text: "TITRES DE BLOC DÉPLIANT",
  classes: "panel-title",
};
Title3Uppercase.storyName = "Titre h3 capitale";

export const Title4 = ({ text, classes }) =>
  `<h4 class="${classes}">${text}</h4>`;
Title4.args = { text: "Titre", classes: "modal-title" };
Title4.storyName = "Titre h4";

export const Paragraph = ({ textParagraph }) => `<p>${textParagraph}</p>`;
Paragraph.storyName = "Paragraphe";

export const ParagraphBold = ({ textParagraph }) =>
  `<p class="texte-courant-semi-gras">${textParagraph}</p>`;
ParagraphBold.storyName = "Paragraphe gras";

export const List = () => `<ul class="liste-styled">
  <li>Item de liste</li>
  <li>Item de liste</li>
  <li>Item de liste</li>
  <li>Item de liste</li>
</ul>
`;
List.storyName = "Liste";
